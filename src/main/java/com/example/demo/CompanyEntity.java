package com.example.demo;

import java.util.List;

public class CompanyEntity {
    private String id;
    private String name;
    private List<UserEntity> employees;
    
    public CompanyEntity(String id, String name, List<UserEntity> employees) {
        super();
        this.id = id;
        this.name = name;
        this.employees = employees;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<UserEntity> getEmployees() {
        return employees;
    }
    public void setEmployees(List<UserEntity> employees) {
        this.employees = employees;
    }
}
