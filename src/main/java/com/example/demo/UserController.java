package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ResponseHeader;


@RestController
public class UserController {

    
    @ExceptionHandler(UserIdNotFoundException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorMessage noUserFound() {
        return new ErrorMessage("User id not found");
    }
    
    @ExceptionHandler(CompanyIdNotFoundException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorMessage noCompanyFound() {
        return new ErrorMessage("Company id not found");
    }
    
    
    public class UserIdNotFoundException extends RuntimeException {
    }
    public class CompanyIdNotFoundException extends RuntimeException {
    }

    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "{ \"message\": \"User id not found\" }") })
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public UserEntity getUser(@PathVariable("id") String id) {
        if (id.equalsIgnoreCase("1")) {
            return new UserEntity("1", "Joe", "Smith");
        }
        throw new UserIdNotFoundException();
    }

    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "{ \"message\": \"Company id not found\" }") })
    @RequestMapping(value = "/company/{id}", method = RequestMethod.GET)
    public CompanyEntity getCompany(@PathVariable("id") String id) {
        if (id.equalsIgnoreCase("1")) {
            List<UserEntity> employees = new ArrayList<UserEntity>(1);
            employees.add(new UserEntity("1", "Joe", "Smith"));
            return new CompanyEntity("1", "demo-company", employees);
        }
        throw new CompanyIdNotFoundException();
    }
}
